import csv
from datetime import datetime
import time
import matplotlib.pyplot as plt
import matplotlib.dates as pltdates
from matplotlib.ticker import FormatStrFormatter


def datetime_from_utc_to_local(utc_datetime):
    epoch = time.mktime(utc_datetime.timetuple())
    offset = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
    return utc_datetime + offset


speed_list = []

# with open('speed_rehberg.csv') as csvfile:
with open('speed_a1_rehberg.csv') as csvfile:
    speed = csv.reader(csvfile, delimiter=',')

    for row in speed:
        speed_list.append(row)

datasets = len(speed_list) - 1
print(datasets)

time_list = []
download_list = []
upload_list = []

for i in range(datasets):
    row = speed_list[1:][i]
    zeit = datetime_from_utc_to_local(datetime.strptime(row[3],
                                      '%Y-%m-%dT%H:%M:%S.%fZ'))
    download = float(row[6]) / 8552996.9 * 8
    upload = float(row[7]) / 8552996.9 * 8
    print(zeit, download, upload)
    time_list.append(zeit)
    download_list.append(download)
    upload_list.append(upload)

# print(time_list)
# print(download_list)

dates = pltdates.date2num(time_list)
plt.plot_date(dates, download_list)
plt.plot_date(dates, upload_list)
plt.gcf().autofmt_xdate()
plt.gca().yaxis.set_major_formatter(FormatStrFormatter('%d Mb/s'))
plt.show()

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from os.path import basename
from email.mime.application import MIMEApplication

fromaddr = "roland.gsell@gmx.at"
toaddr = "roland.gsell@gmx.net"
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "Python email"
filename = 'speedtest.csv'

body = "Python test mail"
msg.attach(MIMEText(body, 'plain'))

with open(filename, "rb") as fil:
    part = MIMEApplication(fil.read(), Name=basename(filename))

part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename)
msg.attach(part)

server = smtplib.SMTP('mail.gmx.net', 587)

server.ehlo()
server.starttls()
server.login(fromaddr, "S3cr3t")

text = msg.as_string()

server.sendmail(fromaddr, toaddr, text)
server.quit
